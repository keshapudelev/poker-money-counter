# Poker Money Counter

[![pipeline status](https://gitlab.com/keshapudelev/poker-money-counter/badges/master/pipeline.svg)](https://gitlab.com/keshapudelev/poker-money-counter/pipelines)

## MVP

- [x] Start game
- [ ] Chip rate
- [x] Chips purchase
- [x] Selling chips
- [ ] Calculation at the end of the game
- [x] Output current statistics
- [ ] Purchase and sale from the admin

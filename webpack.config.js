const dotenv = require('dotenv');
dotenv.config();
const path = require('path');

module.exports = {
  entry: {
    main: './src/index.ts',
  },
  resolve: {
    extensions: ['.js', '.ts'],
    fallback: {
      fs: false,
      net: false,
    },
  },
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        loader: 'ts-loader',
      },
    ],
  },
  target: 'node14',
  optimization: {
    minimize: true,
  },
  experiments: {
    syncWebAssembly: true,
  },
};

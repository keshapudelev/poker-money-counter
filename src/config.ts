import 'dotenv/config';

const config = {
  token: process.env.TELEGRAM_API_TOKEN,
  mongoUrl: process.env.MONGO_URL,
  devUserId: process.env.MY_ID,
  devMode: process.env.DEV_MODE,
};

export default config;

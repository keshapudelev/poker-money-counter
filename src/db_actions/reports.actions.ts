import {
  CurrentReport,
  ReportEntity,
  UserSimpleReport,
  gameModel,
  statusModel,
  transactionRichModel,
} from '../db_models';
import { Transaction, transactionModel } from '../db_models/transactions.model';
import { ReportLastIn, UserGameReport } from './../db_models/reports.model';
import { getUsersStr } from './games.actions';
import TelegramBot = require('node-telegram-bot-api');

export const getUserReport = async (gameId: number, userId: number) => {
  return new Promise<UserSimpleReport | boolean>(async (resolve, reject) => {
    let transactionsFounded = false;
    let userReport = {
      in: null,
      out: null,
      balance: null,
    };
    try {
      await transactionRichModel
        .find({ gameId, user: userId })
        .exec()
        .then((transactions) => {
          const actions: Transaction[] = transactions;
          if (actions && actions.length) {
            console.log('%cFounded transactions', 'color: cyan');
            transactionsFounded = true;
            const _countTransaction = (
              array: Transaction[],
              actionType: 'in' | 'out'
            ) => {
              let res = 0;
              for (let index = 0; index < array.length; index++) {
                const val = array[index];
                if (val.actionType === actionType) {
                  res += val.value;
                }
              }
              return res;
            };
            const inVal = _countTransaction(actions, 'in');
            const outVal = _countTransaction(actions, 'out');
            userReport = {
              in: inVal,
              out: outVal,
              balance: Number(outVal * 1 - inVal * 1),
            };
          } else {
            console.log('%cNo transactions [1]', 'color: red');
            return reject(false);
          }
        })
        .catch((err) => {
          console.log('%cNo transactions [2]\n', 'color: red', err);
          return reject(false);
        });
      if (!transactionsFounded) {
        console.error('%cNo transactions [3]\n', 'color: red');
        return reject(false);
      }
      return resolve(userReport);
    } catch (error) {
      return reject(false);
    }
  });
};

export const getUserPurchases = async (gameId: number, userId: number) => {
  return new Promise<Transaction[] | boolean>(async (resolve, reject) => {
    await transactionRichModel
      .find({ gameId, user: userId, actionType: 'in' })
      .exec()
      .then(async (transactions) => {
        if (!transactions || !transactions.length) {
          return reject(false);
        }
        return resolve(transactions);
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.error(err);
        return reject(false);
      });
  });
};

export const getGameReport = async (
  gameId: number,
  gameSilent?: boolean,
  forceSilentParam?: boolean
) => {
  return new Promise<CurrentReport | boolean | string>(
    async (resolve, reject) => {
      let gameReport: CurrentReport = null;
      try {
        await transactionRichModel
          .find({ gameId })
          .exec()
          .then(async (transactions) => {
            let users: number[] = [];
            let ins: number[] = [];
            let outs: number[] = [];
            let balances: number[] = [];
            let reportEntities: ReportEntity[] = [];
            let lastTransaction: ('in' | 'out')[] = [];
            let lastIns: ReportLastIn[] = [];
            if (transactions && transactions.length) {
              console.log('%cFounded transactions', 'color: cyan');
              lastIns =
                transactions
                  .filter((x: Transaction) => x.actionType === 'in')
                  .slice(-5)
                  .map((transaction): ReportLastIn => {
                    const name = transaction.user.toString();
                    const user = transaction.user;
                    const time = new Date(transaction.dateTime)
                      .toLocaleTimeString('RU-ru')
                      .split(':')
                      .slice(0, 2)
                      .join(':');
                    return {
                      name,
                      user,
                      in: transaction.value,
                      time,
                    };
                  }) || [];
              for (let index = 0; index < transactions.length; index++) {
                const transaction: Transaction = transactions[index];
                let i = null;
                if (!users.includes(transaction.user)) {
                  users.push(transaction.user);
                  i = users.length - 1;
                  ins[i] = 0;
                  outs[i] = NaN;
                  balances[i] = 0;
                } else {
                  i = users.indexOf(transaction.user);
                }
                switch (transaction.actionType) {
                  case 'in':
                    ins[i] += transaction.value;
                    lastTransaction[i] = transaction.actionType;
                    break;
                  case 'out':
                    outs[i] = isNaN(outs[i])
                      ? transaction.value
                      : outs[i] + transaction.value;
                    lastTransaction[i] = transaction.actionType;
                    break;

                  default:
                    break;
                }
                balances[i] = isNaN(outs[i])
                  ? Number(0 - ins[i])
                  : Number(outs[i] * 1 - ins[i]);
              }
            } else {
              console.log('%cNo transactions [4]', 'color: red');
              return reject('No transactions');
            }

            // eslint-disable-next-line
            console.log('gameSilent', gameSilent);

            const names = await getUsersStr(
              users,
              lastTransaction.map((x) =>
                forceSilentParam
                  ? gameSilent
                    ? true
                    : false
                  : x === 'out'
                  ? true
                  : false
              )
              // users.map(() => (gameSilent ? true : false))
            );

            const namesForLastIns = await getUsersStr(
              lastIns.map((x) => x.user),
              lastIns.map(() => true)
            );

            for (let index = 0; index < lastIns.length; index++) {
              const element = lastIns[index];
              const names = namesForLastIns[index];
              element.name = names || 'noname';
            }

            const queryStatuses = await statusModel
              .find({ gameId, userId: { $in: users } })
              .exec();
            // eslint-disable-next-line
            console.log('users', users);
            const statuses = users.map(
              (userId) =>
                queryStatuses.filter((doc) => doc.userId === userId)[0].status
            );

            for (let index = 0; index < users.length; index++) {
              reportEntities.push({
                name: names[index],
                userId: users[index],
                summaryIn: ins[index],
                summaryOut: outs[index],
                summary: balances[index],
                status: statuses[index],
              });
            }
            gameReport = {
              entities: reportEntities,
              lastIns: lastIns,
            };
            return resolve(gameReport);
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.error(err);
            return reject(err);
          });
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        return reject(false);
      }
    }
  );
};

export const getFullUserReport = async (userId: number) => {
  return new Promise<UserSimpleReport | boolean>(async (resolve, reject) => {
    let transactionsFounded = false;
    let userReport = {
      in: null,
      out: null,
      balance: null,
    };
    try {
      await transactionRichModel
        .find({ user: userId })
        .exec()
        .then((transactions) => {
          const actions: Transaction[] = transactions;
          if (actions && actions.length) {
            console.log('%cFounded transactions', 'color: cyan');
            transactionsFounded = true;
            const _countTransaction = (
              array: Transaction[],
              actionType: 'in' | 'out'
            ) => {
              let res = 0;
              for (let index = 0; index < array.length; index++) {
                const val = array[index];
                if (val.actionType === actionType) {
                  res += val.value;
                }
              }
              return res;
            };
            const inVal = _countTransaction(actions, 'in');
            const outVal = _countTransaction(actions, 'out');
            userReport = {
              in: inVal,
              out: outVal,
              balance: Number(outVal * 1 - inVal * 1),
            };
          } else {
            console.log('%cNo transactions [1]', 'color: red');
            return reject(false);
          }
        })
        .catch((err) => {
          console.log('%cNo transactions [2]\n', 'color: red', err);
          return reject(false);
        });
      if (!transactionsFounded) {
        console.error('%cNo transactions [3]\n', 'color: red');
        return reject(false);
      }
      return resolve(userReport);
    } catch (error) {
      return reject(false);
    }
  });
};

export const getFullChatUserReport = async (chatId: number, userId: number) => {
  return new Promise<UserSimpleReport | boolean>(async (resolve, reject) => {
    let transactionsFounded = false;
    let userReport = {
      in: null,
      out: null,
      balance: null,
    };
    try {
      await transactionRichModel
        .find({ user: userId, chatId: chatId })
        .exec()
        .then(async (transactions) => {
          const actions = transactions;
          if (actions && actions.length) {
            console.log('%cFounded transactions', 'color: cyan');
            transactionsFounded = true;
            const _countTransaction = (
              array: Transaction[],
              actionType: 'in' | 'out'
            ) => {
              let res = 0;
              for (let index = 0; index < array.length; index++) {
                const val = array[index];
                if (val.actionType === actionType) {
                  res += val.value;
                }
              }
              return res;
            };
            const inVal = _countTransaction(actions, 'in');
            const outVal = _countTransaction(actions, 'out');
            userReport = {
              in: inVal,
              out: outVal,
              balance: Number(outVal * 1 - inVal * 1),
            };
          } else {
            console.log('%cNo transactions [1]', 'color: red');
            return reject(false);
          }
        })
        .catch((err) => {
          console.log('%cNo transactions [2]\n', 'color: red', err);
          return reject(false);
        });
      if (!transactionsFounded) {
        console.error('%cNo transactions [3]\n', 'color: red');
        return reject(false);
      }
      return resolve(userReport);
    } catch (error) {
      return reject(false);
    }
  });
};

export const getLastGameUserReport = async (chatId: number, userId: number) => {
  return new Promise<UserGameReport | boolean>(async (resolve, reject) => {
    let transactionsFounded = false;
    let userReport = {
      in: null,
      out: null,
      balance: null,
      game: null,
    };
    try {
      await transactionRichModel
        // find all transactions form last gameId in this chat for this userId
        .find({ user: userId, chatId: chatId })
        .exec()
        .then(async (transactions) => {
          // set last game id
          const lastGameId = transactions[transactions.length - 1].gameId;

          // get user game
          const userGame = await gameModel
            .findOne({ id: lastGameId, chatId: chatId })
            .exec();

          const actions: Transaction[] = transactions.filter(
            (x) => x.gameId === lastGameId
          );
          if (actions && actions.length) {
            console.log('%cFounded transactions', 'color: cyan');
            transactionsFounded = true;
            const _countTransaction = (
              array: Transaction[],
              actionType: 'in' | 'out'
            ) => {
              let res = 0;
              for (let index = 0; index < array.length; index++) {
                const val = array[index];
                if (val.actionType === actionType) {
                  res += val.value;
                }
              }
              return res;
            };
            const inVal = _countTransaction(actions, 'in');
            const outVal = _countTransaction(actions, 'out');
            userReport = {
              in: inVal,
              out: outVal,
              balance: Number(outVal * 1 - inVal * 1),
              game: userGame,
            };
          } else {
            console.log('%cNo transactions [1]', 'color: red');
            return reject(false);
          }
        })
        .catch((err) => {
          console.log('%cNo transactions [2]\n', 'color: red', err);
          return reject(false);
        });
      if (!transactionsFounded) {
        console.error('%cNo transactions [3]\n', 'color: red');
        return reject(false);
      }
      return resolve(userReport);
    } catch (error) {
      return reject(false);
    }
  });
};

export const getLastMonthUserReport = async (
  chatId: number,
  userId: number
) => {
  return new Promise<UserSimpleReport | boolean>(async (resolve, reject) => {
    let transactionsFounded = false;
    let userReport = {
      in: null,
      out: null,
      balance: null,
    };
    try {
      await transactionRichModel
        // find last month transactions
        .find({
          user: userId,
          chatId: chatId,
          dateTime: {
            $gte: new Date(new Date().setDate(new Date().getDate() - 30)),
          },
        })
        .exec()
        .then(async (transactions) => {
          // filter transactions by gameId with current chatId
          const gamesIds = await gameModel.find({ chatId }).select('id').exec();
          const actions = transactions.filter((x) =>
            gamesIds.find((y) => y.id === x.gameId)
          );
          if (actions && actions.length) {
            console.log('%cFounded transactions', 'color: cyan');
            transactionsFounded = true;
            const _countTransaction = (
              array: Transaction[],
              actionType: 'in' | 'out'
            ) => {
              let res = 0;
              for (let index = 0; index < array.length; index++) {
                const val = array[index];
                if (val.actionType === actionType) {
                  res += val.value;
                }
              }
              return res;
            };
            const inVal = _countTransaction(actions, 'in');
            const outVal = _countTransaction(actions, 'out');
            userReport = {
              in: inVal,
              out: outVal,
              balance: Number(outVal * 1 - inVal * 1),
            };
          } else {
            console.log('%cNo transactions [1]', 'color: red');
            return reject(false);
          }
        })
        .catch((err) => {
          console.log('%cNo transactions [2]\n', 'color: red', err);
          return reject(false);
        });
      if (!transactionsFounded) {
        console.error('%cNo transactions [3]\n', 'color: red');
        return reject(false);
      }
      return resolve(userReport);
    } catch (error) {
      return reject(false);
    }
  });
};

export const getLastYearUserReport = async (chatId: number, userId: number) => {
  return new Promise<UserSimpleReport | boolean>(async (resolve, reject) => {
    let transactionsFounded = false;
    let userReport = {
      in: null,
      out: null,
      balance: null,
    };
    const yearAgo = new Date(
      new Date().setFullYear(new Date().getFullYear() - 1)
    );
    try {
      await transactionRichModel
        // find last month transactions
        .find({
          user: userId,
          chatId: chatId,
          dateTime: {
            $gte: yearAgo,
          },
        })
        .exec()
        .then(async (transactions) => {
          // filter transactions by gameId with current chatId
          const gamesIds = await gameModel.find({ chatId }).select('id').exec();
          const actions = transactions.filter((x) =>
            gamesIds.find((y) => y.id === x.gameId)
          );
          if (actions && actions.length) {
            console.log('%cFounded transactions', 'color: cyan');
            transactionsFounded = true;
            const _countTransaction = (
              array: Transaction[],
              actionType: 'in' | 'out'
            ) => {
              let res = 0;
              for (let index = 0; index < array.length; index++) {
                const val = array[index];
                if (val.actionType === actionType) {
                  res += val.value;
                }
              }
              return res;
            };
            const inVal = _countTransaction(actions, 'in');
            const outVal = _countTransaction(actions, 'out');
            userReport = {
              in: inVal,
              out: outVal,
              balance: Number(outVal * 1 - inVal * 1),
            };
          } else {
            console.log('%cNo transactions [1]', 'color: red');
            return reject(false);
          }
        })
        .catch((err) => {
          console.log('%cNo transactions [2]\n', 'color: red', err);
          return reject(false);
        });
      if (!transactionsFounded) {
        console.error('%cNo transactions [3]\n', 'color: red');
        return reject(false);
      }
      return resolve(userReport);
    } catch (error) {
      return reject(false);
    }
  });
};

import TelegramBot = require('node-telegram-bot-api');
import { PlayerStatusesEmoji, StatusEntity, statusModel } from '../db_models';

export const setUserStatus = (status: StatusEntity) => {
  return new Promise<boolean>(async (resolve, reject) => {
    try {
      await statusModel
        .create({
          ...status,
        })
        .then((result) => {
          // eslint-disable-next-line
          console.log('Status added successfull');
          return resolve(true);
        })
        .catch((err) => {
          // eslint-disable-next-line
          console.error('Error adding status');
          return reject(false);
        });
      return resolve(true);
    } catch (error) {
      // eslint-disable-next-line
      console.error(error);
      return reject(false);
    }
  });
};

export const getUserStatus = (userId: number, gameId: number) => {
  return new Promise<boolean | (StatusEntity & { statusEmoji: string })>(
    async (resolve, reject) => {
      try {
        const queryFind = statusModel.findOne({
          userId,
          gameId,
        });
        await queryFind
          .exec()
          .then((result: StatusEntity) => {
            // eslint-disable-next-line
            console.log('Status found');
            return resolve({
              ...result,
              statusEmoji: PlayerStatusesEmoji[result.status],
            });
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.error('Status NOT found');
            return reject(false);
          });
        return resolve(true);
      } catch (error) {
        // eslint-disable-next-line
        console.error(error);
        return reject(false);
      }
    }
  );
};

export const updateUserStatus = (status: StatusEntity) => {
  return new Promise<boolean | StatusEntity>(async (resolve, reject) => {
    try {
      const queryUpdateOne = statusModel.findOneAndUpdate(
        { userId: status.userId, gameId: status.gameId },
        {
          status: status.status,
        }
      );
      await queryUpdateOne
        .exec()
        .then((resultStatus) => {
          // eslint-disable-next-line
          console.log(
            `Status updated to ${resultStatus.status} (incoming ${status.status})`
          );
          return resolve(resultStatus);
        })
        .catch((err) => {
          // eslint-disable-next-line
          console.error(err);
          return reject(false);
        });
    } catch (error) {
      // eslint-disable-next-line
      console.error(error);
      return reject(false);
    }
  });
};

export const updateOrAddStatus = (status: StatusEntity) => {
  return new Promise(async (resolve, reject) => {
    try {
      await getUserStatus(status.userId, status.gameId)
        .then(async (result) => {
          await updateUserStatus(status)
            .then((result) => {
              return resolve(result);
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
              return reject(false);
            });
        })
        .catch(async (err) => {
          await setUserStatus(status)
            .then(async (result) => {
              return resolve(true);
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
              return resolve(false);
            });
        });
    } catch (error) {
      // eslint-disable-next-line
      console.error(error);
      return reject(false);
    }
  });
};

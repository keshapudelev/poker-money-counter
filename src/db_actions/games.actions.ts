import TelegramBot = require('node-telegram-bot-api');
import {
  User,
  Game,
  gameModel,
  PlayerStatuses,
  CurrentReport,
} from '../db_models';
import { getUserById } from './users.actions';
import { setUserStatus } from './statuses.actions';
import { getGameReport, getLastGameUserReport } from './reports.actions';
import config from '../config';

let devMode = config.devMode === 'true' ? true : false;

const devId = Number(config.devUserId);

export const getAdminsStr = async (game: Game) => {
  let adminsArray: User[] = [];
  for (let index = 0; index < game.admins.length; index++) {
    const adminId = game.admins[index];
    await getUserById(adminId)
      .then((admin) => {
        adminsArray.push(admin);
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.error(err);
      });
  }
  const adminMentionNames = adminsArray.map(
    (admin) => `[@${admin.name}](tg://user?id=${admin.id})`
  );
  return adminMentionNames;
};

export const getUsersStr = async (usersIds: number[], silent?: boolean[]) => {
  let usersArray = [];
  for (let index = 0; index < usersIds.length; index++) {
    const userId = usersIds[index];
    await getUserById(userId)
      .then((user) => {
        usersArray.push(user);
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.error(err);
      });
  }
  let usersMentionNames: string[] = [];
  if (silent && silent.length) {
    usersMentionNames = usersArray.map((user, i) =>
      silent[i]
        ? `[${user.name}](tg://user?id=${user.id})`
        : `[@${user.name}](tg://user?id=${user.id})`
    );
  } else {
    usersMentionNames = usersArray.map(
      (user) => `[@${user.name}](tg://user?id=${user.id})`
    );
  }
  return usersMentionNames;
};

export const getGameById = async (gameId: number) => {
  return await gameModel
    .findOne({ id: gameId })
    .exec()
    .then((game) => {
      return game;
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
    });
};

export const getLastGameInChat = async (chatId: number) => {
  return await gameModel
    .findOne(
      { chatId: chatId, endTime: { $exists: true } },
      {},
      { sort: { endTime: -1 } }
    )
    .exec()
    .then((game) => {
      return game;
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
    });
};

/**
 * startNewGame
 *
 * @param {TelegramBot.Message} msg
 * @returns
 */
export const startNewGame = async (msg: TelegramBot.Message) => {
  const userId = msg.from.id;
  const msgId = msg.message_id;
  const chatId = msg.chat.id;
  const currentTime = new Date();
  return new Promise<void | Game>(async (resolve, reject) => {
    let gameFounded = false;
    let gameStored: Game = null;
    let startedGame: Game = null;
    try {
      const queryLiveGame = gameModel.findOne({
        chatId: chatId,
        startTime: { $lt: currentTime },
        endTime: { $exists: false },
      });
      await queryLiveGame
        .exec()
        .then((liveGame) => {
          if (liveGame && liveGame.chatId === chatId) {
            console.log('Founded live game');
            gameFounded = true;
            gameStored = liveGame;
            // eslint-disable-next-line
            console.log(gameStored);
          } else {
            console.log('New game');
          }
        })
        .catch((err) => {
          console.log('not found\n', err);
        });
      if (!gameFounded) {
        await gameModel.create(
          {
            id: chatId + msgId,
            admins: [userId],
            chatId,
            startTime: currentTime,
            startMessageId: msgId,
          },
          async function (err, created) {
            if (err) {
              console.error('Error starting game:\n', err);
              return err;
            } else {
              startedGame = created;
              await setUserStatus({
                userId,
                gameId: startedGame.id,
                status: PlayerStatuses.default,
              });
              console.log('Game started!\n', created);
            }
          }
        );
        // return resolve(startedGame);
      } else {
        const adminMentionNames = await getAdminsStr(gameStored);
        const startGameMessageId = gameStored.startMessageId;
        return reject({
          admins: adminMentionNames,
          startMessage: startGameMessageId,
        });
      }
      return resolve(startedGame);
    } catch (error) {
      // eslint-disable-next-line
      console.log('error');
      return reject(error);
    }
  });
};

/**
 * stopGame
 *
 * @param {TelegramBot.Message} msg
 * @returns
 */
export const stopGame = async (msg: TelegramBot.Message) => {
  const userId = msg.from.id;
  const chatId = msg.chat.id;
  return new Promise<boolean | Game>(async (resolve, reject) => {
    let gameFounded = false;
    let gameStored: Game = null;
    try {
      await getLiveGameByChatId(chatId)
        .then((liveGame: Game) => {
          if (liveGame && liveGame.chatId === chatId) {
            console.log('Founded live game');
            gameFounded = true;
            gameStored = liveGame;
            // eslint-disable-next-line
            console.log(gameStored);
          } else {
            console.log('Game not found');
            return reject('no game');
          }
        })
        .catch(() => {
          // eslint-disable-next-line
          console.error('Game not found');
          return reject('no game');
        });
      if (gameFounded) {
        if (gameStored.admins.includes(userId)) {
          return _stopGameById(gameStored.id)
            .then((result) => {
              if (result && result.id) {
                return resolve(gameStored);
              }
              return reject(result);
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
              return reject(err);
            });
        } else {
          let adminsArray: User[] = [];
          for (let index = 0; index < gameStored.admins.length; index++) {
            const adminId = gameStored.admins[index];
            await getUserById(adminId)
              .then((admin) => {
                adminsArray.push(admin);
              })
              .catch((err) => {
                // eslint-disable-next-line
                console.error(err);
              });
          }
          const adminMentionNames = adminsArray.map(
            (admin) => `[@${admin.name}](tg://user?id=${admin.id})`
          );
          const startGameMessageId = gameStored.startMessageId;
          return reject({
            admins: adminMentionNames,
            startMessage: startGameMessageId,
          });
        }
      }
      return reject('no game');
    } catch (error) {
      // eslint-disable-next-line
      console.log(error);
      return reject(error);
    }
  });
};

export const silentGame = async (msg: TelegramBot.Message) => {
  const userId = msg.from.id;
  const chatId = msg.chat.id;
  // eslint-disable-next-line
  console.log('silentGame');
  return new Promise<boolean | Game>(async (resolve, reject) => {
    let gameFounded = false;
    let gameStored: Game = null;
    /* const liveGame =  */ await getLiveGameByChatId(chatId)
      .then((liveGame: Game) => {
        if (liveGame && liveGame.chatId === chatId) {
          console.log('Founded live game');
          gameFounded = true;
          gameStored = liveGame;
          // eslint-disable-next-line
          console.log('>>>', gameStored);
        }
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.log(err);
        reject(false);
      });
    // try {
    if (gameFounded) {
      if (gameStored.admins.includes(userId) || userId === devId) {
        const isSilent = gameStored.silent;
        return _silentGameById(gameStored.id, !isSilent)
          .then((result) => {
            if (result && result.id) {
              return resolve(result);
            } else {
              return reject(false);
            }
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.error(err);
            return reject(err);
          });
      } else {
        let adminsArray: User[] = [];
        for (let index = 0; index < gameStored.admins.length; index++) {
          const adminId = gameStored.admins[index];
          await getUserById(adminId)
            .then((admin) => {
              adminsArray.push(admin);
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
            });
        }
        const adminMentionNames = adminsArray.map(
          (admin) => `[@${admin.name}](tg://user?id=${admin.id})`
        );
        const startGameMessageId = gameStored.startMessageId;
        return reject({
          admins: adminMentionNames,
          startMessage: startGameMessageId,
        });
      }
    }
    // } catch (err) {
    // eslint-disable-next-line
    console.log('try-catch gameFounded - no game');
    return reject('try-catch gameFounded - no game');
    // }
  });
};

export const calcGameRes = async (msg: TelegramBot.Message) => {
  const chatId = msg.chat.id;
  const userId = msg.from.id;
  const text = msg.text;
  const gameId = text?.split(' ')[1];
  return new Promise<boolean | CurrentReport>(async (resolve, reject) => {
    let gameFounded = false;
    let gameStored: Game = null;
    try {
      await getLiveGameByChatId(chatId)
        .then((liveGame: Game) => {
          if (liveGame && liveGame.chatId === chatId) {
            gameFounded = true;
            gameStored = liveGame;
          } else {
            return reject('no game');
          }
        })
        .catch(async () => {
          try {
            let game = null;
            if (gameId) {
              game = await getGameById(Number(gameId));
            } else {
              game = await getLastGameInChat(chatId);
            }
            // eslint-disable-next-line
            console.log('game', game);
            if (game && game.chatId === chatId) {
              gameFounded = true;
              gameStored = game;
            } else {
              return reject('no game');
            }
          } catch (error) {
            // eslint-disable-next-line
            console.error('Game not found');
            return reject('no game');
          }
        });
      if (gameFounded) {
        if (gameStored.admins.includes(userId)) {
          return _calcGameRes(gameStored)
            .then((result) => {
              if (result) {
                return resolve(result as CurrentReport);
              }
              // eslint-disable-next-line
              console.log('no result');
              return reject(false);
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
              return reject(false);
            });
        } else {
          let adminsArray: User[] = [];
          for (let index = 0; index < gameStored.admins.length; index++) {
            const adminId = gameStored.admins[index];
            await getUserById(adminId)
              .then((admin) => {
                adminsArray.push(admin);
              })
              .catch((err) => {
                // eslint-disable-next-line
                console.error(err);
              });
          }
          const adminMentionNames = adminsArray.map(
            (admin) => `[@${admin.name}](tg://user?id=${admin.id})`
          );
          const startGameMessageId = gameStored.startMessageId;
          return reject({
            admins: adminMentionNames,
            startMessage: startGameMessageId,
          });
        }
      }
      return reject('no game');
    } catch (error) {
      // eslint-disable-next-line
      console.log(error);
      reject(error);
    }
  });
};

const _calcGameRes = async (game: Game) => {
  const gameReport = await getGameReport(game.id, game.silent, true);
  // eslint-disable-next-line
  console.log('gameReport', gameReport);
  return gameReport;
};

export const getLiveGameByChatId = async (chatId: number) => {
  const currentTime = new Date();
  return new Promise<Game | boolean>(async (resolve, reject) => {
    try {
      let gameFounded = false;
      let gameStored: Game = null;
      const queryLiveGame = gameModel.findOne({
        chatId: chatId,
        startTime: { $lt: currentTime },
        endTime: { $exists: false },
      });
      try {
        const liveGame = await queryLiveGame.exec();
        if (liveGame && liveGame.chatId === chatId) {
          gameFounded = true;
          gameStored = liveGame;
          return resolve(gameStored);
        } else {
          return reject(false);
        }
      } catch (error) {
        // eslint-disable-next-line
        console.log('Game not found\n', error);
        return reject(false);
      }
    } catch (error) {
      // eslint-disable-next-line
      console.log(error);
      return reject(false);
    }
  });
};

export const getAllLiveGames = async () => {
  const currentTime = new Date();
  return new Promise<Game[] | boolean>(async (resolve, reject) => {
    try {
      let gamesFounded = false;
      let gamesStored: Game[] = null;
      const queryLiveGames = gameModel.find(
        {
          startTime: { $lt: currentTime },
          endTime: { $exists: false },
        },
        {},
        { sort: { startTime: -1 } }
      );
      await queryLiveGames
        .exec()
        .then((liveGames) => {
          if (liveGames && liveGames.length) {
            gamesFounded = true;
            gamesStored = liveGames;
            resolve(gamesStored);
          }
          reject(false);
        })
        .catch((err) => {
          console.log('Games not found\n', err);
          reject(false);
        });
    } catch (error) {
      // eslint-disable-next-line
      console.log(error);
      reject(false);
    }
  });
};

const _stopGameById = async (gameId: number) => {
  const currentTime = new Date();
  return new Promise<Game>(async (resolve, reject) => {
    const queryStopGame = gameModel.findOneAndUpdate(
      { id: gameId },
      {
        endTime: currentTime,
      }
    );
    await queryStopGame
      .exec()
      .then((game) => {
        const updatedGame = game;
        return resolve(updatedGame);
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.error(err);
        return reject(false);
      });
  });
};

const _silentGameById = async (gameId: number, val: boolean = false) => {
  return new Promise<Game>(async (resolve, reject) => {
    const querySilentGame = gameModel.findOneAndUpdate(
      { id: gameId },
      {
        silent: val,
      }
    );
    await querySilentGame
      .exec()
      .then((game) => {
        const updatedGame = game;
        // eslint-disable-next-line
        console.log('updatedGame', updatedGame);
        return resolve(updatedGame);
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.error(err);
        return reject(false);
      });
  });
};

export const forceStopGame = async (gameId: number) => {
  return _forceStopGameById(gameId);
};

const _forceStopGameById = async (gameId: number) => {
  return new Promise<Game>(async (resolve, reject) => {
    const currentTime = new Date();
    // const queryStopGame = gameModel.findOneAndUpdate(
    //   { id: gameId },
    //   {
    //     endTime: new Date(),
    //   }
    // );
    const queryLiveGames = gameModel.find({
      startTime: { $lt: currentTime },
      endTime: { $exists: false },
    });
    await queryLiveGames
      .exec()
      .then(async (games) => {
        if (games.map((x) => x.id).includes(gameId)) {
          const updatedGame = await _stopGameById(gameId);
          return resolve(updatedGame);
        } else {
          // eslint-disable-next-line
          console.error('Game not found');
          return reject('Game not found');
        }
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.error(err);
        return reject(false);
      });
  });
};

export const stopOldGames = async () => {
  // stop games witch have startTime more than 1 months ago
  const stopTime = new Date();
  stopTime.setMonth(stopTime.getMonth() - 1);
  // stopTime.setTime(stopTime.getTime() - 1 * 60 * 1000);
  return new Promise(async (resolve, reject) => {
    const queryStopGames = gameModel.find({
      startTime: { $lt: stopTime },
      endTime: { $exists: false },
    });
    await queryStopGames
      .exec()
      .then(async (games) => {
        const stoppedGamesArray = [];
        if (games.length) {
          for (let index = 0; index < games.length; index++) {
            const game = games[index];
            await _stopGameById(game.id);
            stoppedGamesArray.push(game.id);
          }
        }
        if (stoppedGamesArray.length) {
          return resolve(stoppedGamesArray);
        } else {
          return reject('No games to stop');
        }
      })
      .catch((err) => {
        // eslint-disable-next-line
        console.error(err);
        return reject(false);
      });
  });
};

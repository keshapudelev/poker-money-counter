import TelegramBot = require('node-telegram-bot-api');
import { User, userModel } from '../db_models';

export const addOrUpdateUser = async (msg: TelegramBot.Message) => {
  const userId = msg.from.id;
  const userName = msg.from.first_name;
  const userNickname = msg.from.username;
  const resName = userNickname ? userNickname : userName;
  return new Promise<void>(async (resolve, reject) => {
    let userFounded = false;
    let userStored: User = null;
    let userStoredId = null;
    const queryFind = userModel.findOne({ id: userId }, 'id name');
    try {
      await queryFind
        .exec()
        .then((userFromDb) => {
          if (userFromDb && userFromDb.id === userId) {
            console.log(
              '%cFounded user\n%cOld User',
              'color: cyan',
              'color: red'
            );
            userFounded = true;
            userStored = { id: userFromDb.id, name: userFromDb.name };
            userStoredId = userFromDb.id;
          } else {
            console.log('%cNew user', 'color: green');
            userFounded = false;
            userStored = null;
            userStoredId = null;
          }
        })
        .catch((err) => {
          console.log('%cNot found\n', 'color: red', err);
          userFounded = false;
          userStored = null;
          userStoredId = null;
        });
      if (!userFounded) {
        await userModel.create(
          { id: userId, name: resName },
          function (err, created) {
            if (err) {
              console.error('%cError adding user:\n', 'color: red', err);
              return err;
            } else {
              console.log('%cUser added!\n', 'color: green', created);
            }
          }
        );
      } else {
        if (userStored.name !== resName) {
          const queryUpdateOne = userModel.findOneAndUpdate(userStored, {
            name: resName,
          });
          await queryUpdateOne
            .exec()
            .then((resultUser) => {
              console.log('%cUser name updated!\n', 'color: green', resultUser);
            })
            .catch((err) => {
              console.error('%cError updating user name:\n', 'color: red', err);
            });
        }
      }
      return resolve();
    } catch (error) {
      return reject(error);
    }
  });
};

/**
 * getUserById
 *
 * @param {number} id
 * @returns
 */
export const getUserById = async (id: number) => {
  return new Promise<User>(async (resolve, reject) => {
    try {
      let foundedUser: User = null;
      const queryUser = userModel.findOne({ id });
      await queryUser.exec().then((user) => {
        foundedUser = user;
      });
      return resolve(foundedUser);
    } catch (error) {
      return reject(error);
    }
  });
};

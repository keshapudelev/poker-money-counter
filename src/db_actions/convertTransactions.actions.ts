import {
  TransactionRich,
  gameModel,
  transactionModel,
  transactionRichModel,
} from '../db_models';
import { oldTransactionsTransferredModel } from '../db_models/oldTransactionsTransferred.model';

const convertTransactions = async () => {
  const query = transactionModel.find();

  await query.exec().then(async (res) => {
    // add chatId from gameModel for every entry
    const transactions = res;
    let isMigrated = false;
    try {
      const migrated = await oldTransactionsTransferredModel.findOne().exec();
      isMigrated = migrated?.transferred;
    } catch (error) {
      isMigrated = false;
    }
    if (transactions && transactions.length && !isMigrated) {
      // eslint-disable-next-line
      console.log('go migrate!');
      await transactionRichModel.deleteMany().exec();
      transactions.forEach(async (transaction) => {
        await gameModel
          .findOne({ id: transaction.gameId })
          .exec()
          .then(async (game) => {
            const transactionRich = new TransactionRich({
              id: transaction.id,
              actionType: transaction.actionType,
              dateTime: transaction.dateTime,
              gameId: transaction.gameId,
              messageId: transaction.messageId,
              user: transaction.user,
              value: transaction.value,
              // new value
              chatId: game.chatId,
            });
            await transactionRichModel.create(transactionRich);
          });
      });
      await oldTransactionsTransferredModel.create({ transferred: true });
    } else {
      // eslint-disable-next-line
      console.log('already migrated');
    }
  });
};

export default convertTransactions;

import TelegramBot = require('node-telegram-bot-api');
import {
  PlayerStatuses,
  StatusEntity,
  TransactionRich,
  transactionRichModel,
} from '../db_models';
import { updateOrAddStatus } from './statuses.actions';

export const getGameTransactions = async (game_id: number) => {
  return new Promise<TransactionRich[] | boolean>(async (resolve, reject) => {
    try {
      const query = transactionRichModel.find({ gameId: game_id });
      await query.exec().then((res) => {
        const transactions = res;
        if (transactions && transactions.length) {
          resolve(transactions);
        } else {
          reject(false);
        }
      });
    } catch (error) {
      // eslint-disable-next-line
      console.error(error);
      reject(error);
    }
  });
};

export const addChips = async (
  gameId: number,
  value: number,
  user: number,
  messageId: number,
  chatId: number
) => {
  const transaction: TransactionRich = new TransactionRich({
    actionType: 'in',
    gameId,
    user,
    value,
    messageId,
    chatId,
  });
  return new Promise<void>(async (resolve, reject) => {
    try {
      await transactionRichModel.create(
        transaction,
        async function (err, created) {
          if (err) {
            console.error('%cError adding chips:\n', 'color: red', err);
            return err;
          } else {
            console.log('%cChips added!\n', 'color: green', created);
            await updateOrAddStatus({
              userId: user,
              gameId,
              status: PlayerStatuses.active,
            });
            return;
          }
        }
      );
      return resolve();
    } catch (error) {
      return reject(error);
    }
  });
};

export const withdrawalChips = async (
  gameId: number,
  value: number,
  user: number,
  messageId,
  chatId: number
) => {
  const transaction: TransactionRich = new TransactionRich({
    actionType: 'out',
    gameId,
    user,
    value,
    messageId,
    chatId,
  });
  return new Promise<void | boolean>(async (resolve, reject) => {
    try {
      await transactionRichModel.create(
        transaction,
        async function (err, created) {
          if (err) {
            console.error('%cError withdrawal:\n', 'color: red', err);
            return reject(err);
          } else {
            console.log('%cWithdrawal success!\n', 'color: green', created);
            await updateOrAddStatus({
              userId: user,
              gameId,
              status: PlayerStatuses.ended,
            });
          }
        }
      );
      return resolve();
    } catch (error) {
      return reject(false);
    }
  });
};

export const deleteTransaction = async (gameId: number, messageId: number) => {
  return new Promise<void | boolean>(async (resolve, reject) => {
    try {
      let success = false;
      const deleteQuery = transactionRichModel.findOneAndDelete({
        gameId,
        messageId,
      });
      await deleteQuery
        .exec()
        .then((res) => {
          // eslint-disable-next-line
          console.log(res);
          if (!res) {
            console.error('%cError deleting transaction:\n', 'color: red');
            success = false;
          } else {
            console.error('%cTransaction deleted!\n', 'color: red');
            success = true;
          }
        })
        .catch((err) => {
          console.error('%cError deleting transaction:\n', 'color: red', err);
          success = false;
        });
      if (success) {
        // eslint-disable-next-line
        console.log(success);
        return resolve();
      } else {
        // eslint-disable-next-line
        console.log(success);
        return reject(false);
      }
    } catch (error) {
      // eslint-disable-next-line
      console.log(error);
      return reject(false);
    }
  });
};

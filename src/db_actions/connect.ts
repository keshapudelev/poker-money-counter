import * as mongoose from 'mongoose';
import config from '../config';

type mongooseType = typeof mongoose;
// export mongoose;

// Установим подключение по умолчанию
var mongoDB = config.mongoUrl;
mongoose.connect(mongoDB).then(() => {
  console.info.bind(console, 'MongoDB connected');
});

export const myMongoose = mongoose;

// Позволим Mongoose использовать глобальную библиотеку промисов
(<mongooseType>myMongoose).Promise = global.Promise;

// Получение подключения по умолчанию
export const db = myMongoose.connection;
// Привязать подключение к событию ошибки  (получать сообщения об ошибках подключения)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

db.on('connected', console.info.bind(console, 'MongoDB connected'));

myMongoose.set('returnOriginal', false);

// const handleError = (error) => {
//   console.error.bind(console, 'MongoDB connection error:', error);
// };

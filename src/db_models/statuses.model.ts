// import { PlayerStatuses } from './../index';
import { myMongoose } from '../db_actions/connect';

export enum PlayerStatusesEmoji {
  default = '❔',
  active = '🟢',
  ended = '🔴',
  payedCard = '💳',
  payedCash = '💵',
}

export enum PlayerStatuses {
  default = 'default',
  active = 'active',
  ended = 'ended',
  payedCard = 'payedCard',
  payedCash = 'payedCash',
}

export type StatusEntity = {
  gameId: number;
  userId: number;
  status: PlayerStatuses;
  _id?: string;
};

export const statusSchema = new myMongoose.Schema({
  gameId: {
    type: Number,
    required: true,
  },
  userId: {
    type: Number,
    required: true,
  },
  status: {
    type: String,
    enum: PlayerStatuses,
    required: true,
  },
});

export const statusModel = myMongoose.model<StatusEntity>(
  'statusModel',
  statusSchema
);

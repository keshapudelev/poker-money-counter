import { myMongoose } from '../db_actions/connect';

export interface Game {
  id: number;
  admins: number[];
  chatId: number;
  endTime?: Date;
  startTime: Date;
  startMessageId: number;
  silent?: boolean;
}

export const gameSchema = new myMongoose.Schema({
  id: {
    type: Number,
    unique: true,
    index: true,
    required: true,
  },
  admins: {
    type: [Number],
    required: true,
  },
  chatId: {
    type: Number,
    required: true,
  },
  endTime: {
    type: Date,
  },
  startTime: {
    type: Date,
    required: true,
  },
  startMessageId: Number,
  silent: {
    type: Boolean,
  },
});

export const gameModel = myMongoose.model<Game>('gameModel', gameSchema);

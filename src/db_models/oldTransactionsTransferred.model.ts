import { myMongoose } from '../db_actions/connect';

export const oldTransactionsTransferredSchema = new myMongoose.Schema({
  transferred: Boolean,
});

export const oldTransactionsTransferredModel = myMongoose.model<{
  transferred: boolean;
}>('oldTransactionsTransferredModel', oldTransactionsTransferredSchema);

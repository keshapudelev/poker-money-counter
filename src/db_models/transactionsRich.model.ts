import { myMongoose } from '../db_actions/connect';
import { TransactionAction, iTransaction } from './transactions.model';

export interface iTransactionRich extends iTransaction {
  chatId: number;
}

export interface TransactionRichAction extends TransactionAction {
  chatId: number;
  id?: number;
}

export class TransactionRich implements TransactionRichAction {
  id = null;
  dateTime = null;

  actionType: 'in' | 'out' = null;
  gameId: number = null;
  user: number = null;
  value: number = null;
  messageId: number = null;
  timestamp: string = null;
  chatId: number = null;

  constructor(data: TransactionRichAction) {
    this.actionType = data.actionType;
    this.gameId = data.gameId;
    this.user = data.user;
    this.value = data.value;
    this.messageId = data.messageId;
    this.chatId = data.chatId;

    this.dateTime = data.dateTime ? data.dateTime : new Date();
    this.timestamp = new Date()
      .toLocaleTimeString('RU-ru')
      .split(':')
      .slice(0, 2)
      .join(':');

    this.id = data.id ? data.id : this.gameId + this.dateTime.getTime();
  }
}

export const transactionRichSchema = new myMongoose.Schema({
  id: {
    type: Number,
    unique: true,
    index: true,
    required: true,
  },
  actionType: {
    type: String,
    required: true,
  },
  dateTime: {
    type: Date,
    required: true,
  },
  gameId: {
    type: Number,
    required: true,
  },
  messageId: {
    type: Number,
    required: true,
  },
  user: {
    type: Number,
    required: true,
  },
  value: {
    type: Number,
    required: true,
  },
  chatId: {
    type: Number,
    required: true,
  },
});

export const transactionRichModel = myMongoose.model<TransactionRich>(
  'transactionRichModel',
  transactionRichSchema
);

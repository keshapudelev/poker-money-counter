import { myMongoose } from '../db_actions/connect';

export interface User {
  id: number;
  name: string;
}

export const userSchema = new myMongoose.Schema({
  id: {
    type: Number,
    unique: true,
    index: true,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
});

export const userModel = myMongoose.model<User>('userModel', userSchema);

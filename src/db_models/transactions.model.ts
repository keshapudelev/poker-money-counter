import { myMongoose } from '../db_actions/connect';

export interface iTransaction {
  actionType: 'in' | 'out';
  dateTime: Date;
  gameId: number;
  id: number;
  user: number;
  value: number;
  messageId: number;
}

export interface TransactionAction {
  actionType: 'in' | 'out';
  gameId: number;
  user: number;
  value: number;
  messageId: number;
  dateTime?: string;
}

export class Transaction implements TransactionAction {
  id = null;
  dateTime = null;

  actionType: 'in' | 'out' = null;
  gameId: number = null;
  user: number = null;
  value: number = null;
  messageId: number = null;
  timestamp: string = null;

  constructor(data: TransactionAction) {
    this.actionType = data.actionType;
    this.gameId = data.gameId;
    this.user = data.user;
    this.value = data.value;
    this.messageId = data.messageId;

    this.dateTime = new Date();
    this.timestamp = new Date()
      .toLocaleTimeString('RU-ru')
      .split(':')
      .slice(0, 2)
      .join(':');

    this.id = this.gameId + this.dateTime.getTime();
  }
}

export const transactionSchema = new myMongoose.Schema({
  id: {
    type: Number,
    unique: true,
    index: true,
    required: true,
  },
  actionType: {
    type: String,
    required: true,
  },
  dateTime: {
    type: Date,
    required: true,
  },
  gameId: {
    type: Number,
    required: true,
  },
  messageId: {
    type: Number,
    required: true,
  },
  user: {
    type: Number,
    required: true,
  },
  value: {
    type: Number,
    required: true,
  },
});

export const transactionModel = myMongoose.model<Transaction>(
  'transactionModel',
  transactionSchema
);

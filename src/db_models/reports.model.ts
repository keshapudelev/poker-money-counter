// import { myMongoose } from '../db_actions/connect';
import { Game } from './games.model';
import { PlayerStatuses } from './statuses.model';

export type ReportEntity = {
  name: string;
  userId: number;
  summaryIn: number;
  summaryOut: number;
  summary: number;
  status: PlayerStatuses;
};

export type ReportLastIn = {
  name?: string;
  user: number;
  in: number;
  time: string;
};
export interface CurrentReport {
  entities: ReportEntity[];
  lastIns?: ReportLastIn[];
}

export interface UserSimpleReport {
  in: number;
  out: number;
  balance: number;
}

export interface UserReport extends UserSimpleReport {
  name: String;
}

export interface UserGameReport extends UserSimpleReport {
  game: Game;
}

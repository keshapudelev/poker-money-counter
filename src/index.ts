import TelegramBot = require('node-telegram-bot-api');
import { gameCommands } from './commands/';
import config from './config';
import './db_actions/connect';
import { callbackQueryAction } from './commands/callbackQuery';
import convertTransactions from './db_actions/convertTransactions.actions';

export type GameConfig = {
  all_chips: number;
  chips_rate: number;
};

const env = process.env;

const options = {
  polling: true,
};

const bot = new TelegramBot(config.token, options);

// test commands
// messageReaction(bot);
// echoCommand(bot);

// waiting for commands
for (const iterator in gameCommands) {
  gameCommands[iterator](bot);
}

convertTransactions().then(() => {
  callbackQueryAction(bot);
});

import TelegramBot = require('node-telegram-bot-api');
import { addOrUpdateUser } from '../db_actions';
import { User } from '../db_models';

export const echoCommand = (bot: TelegramBot) => {
  // Matches "/echo [whatever]"
  bot.onText(/\/echo (.+)/, async (msg, match) => {
    // 'msg' is the received Message from Telegram
    // 'match' is the result of executing the regexp above on the text content
    // of the message

    await addOrUpdateUser(msg);
    const chatId = msg.chat.id;
    const msgId = msg.message_id;
    const userId = msg.from.id;
    const userName = msg.from.first_name;
    const resp = match[1]; // the captured "whatever"

    // send back the matched "whatever" to the chat
    bot.sendMessage(chatId, resp, {
      reply_to_message_id: msgId,
    });

    // eslint-disable-next-line
    console.log(
      `\n>>> [/echo] chatId:${chatId} userId:${userId} userName:${userName} resp:${resp}`
    );
  });
};

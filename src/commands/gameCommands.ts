const BOT_NAME = '@poker_money_counter_bot';

import TelegramBot = require('node-telegram-bot-api');
import { isArray } from 'util';
import config from '../config';
import {
  addChips,
  addOrUpdateUser,
  deleteTransaction,
  forceStopGame,
  getAdminsStr,
  getFullChatUserReport,
  getFullUserReport,
  getGameReport,
  getGameTransactions,
  getLastGameUserReport,
  getLastMonthUserReport,
  getLastYearUserReport,
  getLiveGameByChatId,
  getUserById,
  getUserPurchases,
  getUserReport,
  startNewGame,
  stopGame,
  withdrawalChips,
} from '../db_actions';
import {
  calcGameRes,
  getAllLiveGames,
  silentGame,
  stopOldGames,
} from '../db_actions/games.actions';
import {
  CurrentReport,
  Game,
  PlayerStatuses,
  PlayerStatusesEmoji,
  ReportEntity,
  TransactionRich,
  UserGameReport,
  UserSimpleReport,
} from '../db_models';

let devMode = config.devMode === 'true' ? true : false;

const devId = Number(config.devUserId);

const removeKeyboard = {
  remove_keyboard: true,
};

const inOutBtns = async (
  chatId: number
): Promise<TelegramBot.InlineKeyboardMarkup> => {
  const res = await getCurrentGamePurchaseBtns(chatId);
  if (res && isArray(res)) {
    return {
      inline_keyboard: [res, [{ text: '⛔️ /out 0', callback_data: 'out_0' }]],
    };
  }
  return {
    inline_keyboard: [[{ text: '⛔️ /out 0', callback_data: 'out_0' }]],
  };
};

const getCurrentGamePurchaseBtns = async (
  chatId: number
): Promise<TelegramBot.InlineKeyboardButton[] | boolean> => {
  const btnsCount = 3;
  if (!chatId) {
    return;
  }
  return await getLiveGameByChatId(chatId)
    .then(async (game: Game) => {
      if (game && game.id) {
        return await getGameTransactions(game.id)
          .then((transactions: TransactionRich[]) => {
            const inTransactions = transactions.filter(
              (x) => x.actionType === 'in'
            );
            const valuesArray = [];
            const countArray = [];
            for (let index = 0; index < inTransactions.length; index++) {
              const value = inTransactions[index].value;
              if (valuesArray.includes(value)) {
                const indexValue = valuesArray.indexOf(value);
                countArray[indexValue] += 1;
              } else {
                valuesArray.push(value);
                countArray.push(1);
              }
            }
            const sortedValues = valuesArray.sort((a, b) => {
              const indexA = valuesArray.indexOf(a);
              const indexB = valuesArray.indexOf(b);
              return countArray[indexB] - countArray[indexA];
            });

            const btnsArray = [];
            for (
              let index = 0;
              index < Math.min(btnsCount, sortedValues.length);
              index++
            ) {
              btnsArray.push({
                text: `📥 /in ${sortedValues[index]}`,
                callback_data: `in_${sortedValues[index]}`,
              });
            }
            return btnsArray;
          })
          .catch((err) => {
            // eslint-disable-next-line
            console.log(err);
            return err;
          });
      } else {
        // eslint-disable-next-line
        console.log('getCurrentGamePurchaseBtns(): smth wend wrong');
        return false;
      }
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
      return false;
    });
};

const _getAdminsStr = (admins: String[]) => {
  if (!admins || !admins.length) {
    return '';
  }
  let resStr = admins.length > 1 ? 'Админы игры:' : 'Админ игры:';
  for (let index = 0; index < admins.length; index++) {
    const admin = admins[index];
    resStr += `${index > 0 ? ', ' : ' '}${admin}`;
  }
  return resStr;
};

const _logCommand = (command: string, msg: TelegramBot.Message) => {
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  const userName = msg.from.first_name;

  // eslint-disable-next-line
  console.log(
    `\n%c>>> %c[%c${command}%c] %cchatId:%c${chatId} %cmsgId:%c${msgId} %cuserId:%c${userId} %cuserName:%c${userName}`,
    'color: red',
    'color: gray',
    'color: brue',
    'color: gray',
    'color: gray',
    'color: white',
    'color: gray',
    'color: white',
    'color: gray',
    'color: white',
    'color: gray',
    'color: white'
  );
};

const RETRY_AFTER = 10;

const _startCommandBody = async (
  bot: TelegramBot,
  msg?: TelegramBot.Message
) => {
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  const userName = msg.from.first_name;

  _logCommand('/start', msg);

  let justHaveGame = '';
  let liveGameMessageId = null;

  await addOrUpdateUser(msg)
    .then(async () => {
      await startNewGame(msg)
        .then((res) => {
          // eslint-disable-next-line
          // console.log('then', res);
        })
        .catch((res) => {
          if (res.admins && res.admins.length) {
            justHaveGame +=
              '⛔️ Игра уже начата.\nДля новой игры надо закончить предыдущую.\n';
            justHaveGame += _getAdminsStr(res.admins);
            justHaveGame += '\nКоманда /stop';
          }
          if (res.startMessage) {
            liveGameMessageId = res.startMessage;
          }
        });
    })
    .catch((error) => {
      // eslint-disable-next-line
      console.log(error);
    });

  const resp = justHaveGame
    ? justHaveGame
    : `Хорошей игры!\n🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢`;

  bot
    .sendMessage(chatId, resp, {
      reply_to_message_id: liveGameMessageId ? liveGameMessageId : msgId,
      reply_markup: justHaveGame
        ? {
            inline_keyboard: [[{ text: '/stop', callback_data: 'stop' }]],
          }
        : {
            inline_keyboard: [
              [
                { text: '/in 500', callback_data: 'in_500' },
                { text: '/in 1000', callback_data: 'in_1000' },
                { text: '/in 5000', callback_data: 'in_5000' },
              ],
            ],
          },
      parse_mode: 'Markdown',
    })
    .catch(() => {
      setTimeout(() => {
        bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            parse_mode: 'Markdown',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp, {
                parse_mode: 'Markdown',
              });
            }, RETRY_AFTER);
          });
      }, RETRY_AFTER);
    });
};

export const startCommandExport = (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  if (!bot || !msg) {
    return;
  }
  return _startCommandBody(bot, msg);
};

export const calcCommandExport = (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  if (!bot || !msg) {
    return;
  }
  return _calcCommand(bot, msg);
};

export const calcCommand = (bot: TelegramBot) => {
  bot.onText(
    /^\/calc(@poker_money_counter_bot)?$/,
    async (msg /* , match */) => {
      const chatId = msg.chat.id;
      const userId = msg.from.id;
      if ((devMode && userId === devId && chatId === devId) || !devMode) {
        _calcCommand(bot, msg);
      } else return false;
    }
  );
};

const _calcCommand = async (bot: TelegramBot, msg: TelegramBot.Message) => {
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  const userName = msg.from.first_name;
  _logCommand('/calc', msg);
  try {
    return calcGameRes(msg)
      .then(async (entities: CurrentReport) => {
        let allSummary = 0;

        for (let index = 0; index < entities.entities.length; index++) {
          allSummary += entities.entities[index].summary;
        }

        if (allSummary !== 0) {
          const resp = `Баланс не сошёлся, рассчитывать не буду.\n\`kill\` не поможет, либо считайте сами, либо подбивайте баланс.`;
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, {
                  parse_mode: 'Markdown',
                });
              }, RETRY_AFTER);
            });
          return;
        }

        const eS: ReportEntity[] = JSON.parse(
          JSON.stringify(
            entities.entities.sort((a, b) => {
              if (a.summary > b.summary) {
                return 1;
              } else return -1;
            })
          )
        );

        // eslint-disable-next-line
        console.log('eS', eS);

        let lI = 0;
        let wI = eS.length - 1;

        let resp = '';

        for (let index = 0; index < eS.length; index++) {
          while (eS[lI].summary != 0) {
            // const gamerNames = await getUsersStr([
            //   eS[lI].userId,
            //   eS[wI].userId,
            // ]);
            if (-eS[lI].summary >= eS[wI].summary) {
              resp +=
                eS[lI].name +
                ' ➡️ ' +
                eS[wI].name +
                ': ' +
                eS[wI].summary +
                '\n';
              eS[lI].summary += eS[wI].summary;
              eS[wI].summary = 0;
              wI -= 1;
            } else {
              resp +=
                eS[lI].name +
                ' ➡️ ' +
                eS[wI].name +
                ': ' +
                -eS[lI].summary +
                '\n';
              eS[wI].summary -= -eS[lI].summary;
              eS[lI].summary = 0;
            }
          }
          lI += 1;
        }

        if (!resp) {
          resp += 'Не получилось рассчитать - не хватает данных';
        }

        bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            parse_mode: 'Markdown',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp, {
                parse_mode: 'Markdown',
              });
            }, RETRY_AFTER);
          });
      })
      .catch((res) => {
        if (Array.isArray(res.admins)) {
          const resp = `У тебя нет прав для старта расчёта.\nПопроси админа.\n${_getAdminsStr(
            res.admins
          )}`;
          return bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, {
                  parse_mode: 'Markdown',
                });
              }, RETRY_AFTER);
            });
        }
      });
  } catch (err) {
    bot
      .sendMessage(chatId, `Не получилось рассчитать :(\n${err}`, {
        reply_to_message_id: msgId,
        parse_mode: 'Markdown',
      })
      .catch(() => {
        setTimeout(() => {
          bot.sendMessage(chatId, `Не получилось рассчитать :(\n${err}`, {
            parse_mode: 'Markdown',
          });
        }, RETRY_AFTER);
      });
    return false;
  }
};

/**
 * startCommand
 *
 * @param {TelegramBot} bot
 */
export const startCommand = (bot: TelegramBot) => {
  // Matches "/start"
  bot.onText(
    /^\/start(@poker_money_counter_bot)?$/,
    async (msg /* , match */) => {
      const chatId = msg.chat.id;
      const userId = msg.from.id;
      if ((devMode && userId === devId && chatId === devId) || !devMode) {
        _startCommandBody(bot, msg);
      } else return false;
    }
  );
};

export const stopCommandExport = async (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  if (!bot || !msg) {
    return;
  }

  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  _logCommand('/stop', msg);

  await _gameReportCommand(bot, msg)
    .then(async (gameBalance) => {
      if (
        gameBalance === 0 ||
        gameBalance === 'No transactions' ||
        gameBalance === 'No live game'
      ) {
        await stopGame(msg)
          .then(() => {
            const resp = `Всем спасибо за игру!\n⛔️⛔️⛔️⛔️⛔️⛔️⛔️⛔️⛔️⛔️`;

            return bot.sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              reply_markup: removeKeyboard,
            });
          })
          .catch((res) => {
            if (res === 'no game') {
              return;
              // const resp = `Нет игры для остановки.\nСначала запустите новую.\n/start`;

              // return bot
              //   .sendMessage(chatId, resp, {
              //     reply_to_message_id: msgId,
              //   })
              //   .catch(() => {
              //     bot.sendMessage(chatId, resp);
              //   });
            } else {
              if (Array.isArray(res.admins)) {
                const resp = `У тебя нет прав для остановки текущей игры.\nПопроси админа остановить игру или добавить тебе права.\n${_getAdminsStr(
                  res.admins
                )}`;
                return bot
                  .sendMessage(chatId, resp, {
                    reply_to_message_id: msgId,
                    parse_mode: 'Markdown',
                  })
                  .catch(() => {
                    setTimeout(() => {
                      bot.sendMessage(chatId, resp, {
                        parse_mode: 'Markdown',
                      });
                    }, RETRY_AFTER);
                  });
              }
            }
          });
      } else {
        // eslint-disable-next-line
        console.log(gameBalance);
        const resp = '⚠ Баланс игры не сошёлся!\nПерепроверьте фишки.';
        return bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            reply_markup: {
              inline_keyboard: [[{ text: '/kill', callback_data: 'kill' }]],
            },
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp);
            }, RETRY_AFTER);
          });
      }
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.log(err);
      return false;
    });
};

/**
 * stopCommand
 *
 * @param {TelegramBot} bot
 */
export const stopCommand = (bot: TelegramBot) => {
  // Matches "/stop"
  bot.onText(/^\/stop(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const msgId = msg.message_id;
    const userId = msg.from.id;

    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      stopCommandExport(bot, msg);
    } else return false;
  });
};

export const silentCommand = (bot: TelegramBot) => {
  bot.onText(/^\/silent(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const msgId = msg.message_id;
    const userId = msg.from.id;
    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _logCommand('/silent', msg);
      try {
        const res: any = await silentGame(msg);
        if (res.silent || res.silent === false) {
          const isSilent = res.silent;
          bot
            .sendMessage(
              chatId,
              `${isSilent ? 'Включен тихий режим' : 'Выключен тихий режим'}`,
              {
                reply_to_message_id: msgId,
                parse_mode: 'Markdown',
              }
            )
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(
                  chatId,
                  `${
                    isSilent ? 'Включен тихий режим' : 'Выключен тихий режим'
                  }`,
                  {
                    parse_mode: 'Markdown',
                  }
                );
              }, RETRY_AFTER);
            });
        }
        // eslint-disable-next-line
        console.log('>>> res', res);
      } catch (error) {
        bot
          .sendMessage(chatId, `нет активной игры`, {
            reply_to_message_id: msgId,
            parse_mode: 'Markdown',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, `нет активной игры`, {
                parse_mode: 'Markdown',
              });
            }, RETRY_AFTER);
          });
      }
    } else return false;
  });
};

const _killCommand = async (bot: TelegramBot, msg: TelegramBot.Message) => {
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  _logCommand('/kill', msg);
  await stopGame(msg)
    .then(() => {
      const resp = `Всем спасибо за игру!\n⛔️⛔️⛔️⛔️⛔️⛔️⛔️⛔️⛔️⛔️`;

      bot.sendMessage(chatId, resp, {
        reply_to_message_id: msgId,
        reply_markup: removeKeyboard,
      });
    })
    .catch((res) => {
      if (res === 'no game') {
        const resp = `Нет игры для остановки.\nСначала запустите новую.\n/start`;

        bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            reply_markup: {
              inline_keyboard: [[{ text: '/start', callback_data: 'start' }]],
            },
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp);
            }, RETRY_AFTER);
          });
      } else {
        if (Array.isArray(res.admins)) {
          const resp = `У тебя нет прав для остановки текущей игры.\nПопроси админа остановить игру или добавить тебе права.\n${_getAdminsStr(
            res.admins
          )}`;
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        }
      }
    });
};

export const killCommandExport = (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  if (!bot || !msg) {
    return;
  }
  return _killCommand(bot, msg);
};

export const killCommand = (bot: TelegramBot) => {
  bot.onText(
    /^\/kill(@poker_money_counter_bot)?$/,
    async (msg /* , match */) => {
      const chatId = msg.chat.id;
      const msgId = msg.message_id;
      const userId = msg.from.id;

      if ((devMode && userId === devId && chatId === devId) || !devMode) {
        _killCommand(bot, msg);
      } else return false;
    }
  );
};

export const forceStopGameCommand = async (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  bot.onText(/\/stopForce(@poker_money_counter_bot)? (.+)/, async (msg) => {
    const chatId = msg.chat.id;
    const msgId = msg.message_id;
    const userId = msg.from.id;
    if (userId !== devId) {
      return false;
    }
    if (/\/stopForce(@poker_money_counter_bot)? (.+)/.test(msg.text)) {
      const match = msg.text.match(
        /\/stopForce(@poker_money_counter_bot)? (.+)/
      );
      // eslint-disable-next-line
      console.log('match', match);
      _logCommand(`/stopForce ${match[2]}`, msg);
      await forceStopGame(Number(match[2]))
        .then((result) => {
          const resp = `Игра ${match[2]} остановлена\n`;
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        })
        .catch((err) => {
          const resp = `Не удалось остановить игру ${match[2]}\n${err}`;
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        });
    }
  });
};

export const forceStopOldGameCommand = async (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  bot.onText(/\/stopOld(@poker_money_counter_bot)?/, async (msg) => {
    const chatId = msg.chat.id;
    const msgId = msg.message_id;
    const userId = msg.from.id;
    if (userId !== devId) {
      return false;
    }
    _logCommand(`/stopOld`, msg);
    await stopOldGames()
      .then((result: number[]) => {
        // eslint-disable-next-line
        console.log('result', result);
        let resp = `Игры старше 1 месяца остановлены!\n`;
        resp += `${result.join('|')}\n`;
        if (resp.length >= 4096) {
          resp = resp.substring(0, 4093) + '...';
        }
        bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            parse_mode: 'Markdown',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
            }, RETRY_AFTER);
          });
      })
      .catch((err) => {
        const resp = `Не удалось остановить игры\n${err}`;
        bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            parse_mode: 'Markdown',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
            }, RETRY_AFTER);
          });
      });
  });
};

const _inCommand = async (
  bot: TelegramBot,
  msg: TelegramBot.Message,
  str: string
) => {
  if (/\/in (.+)/.test(str)) {
    const match = str.match(/\/in (.+)/);
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const msgId = msg.message_id;
    _logCommand(`/in ${match[1]}`, msg);

    await getLiveGameByChatId(chatId)
      .then(async (liveGame: Game) => {
        const gameId = liveGame.id;
        const _badValue = () => {
          const resp = 'Неверное значение';
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        };

        const chips = Math.floor(Number(match[1]));
        if (isNaN(chips) || chips <= 0 || chips > 999999) {
          return _badValue();
        } else {
          await addOrUpdateUser(msg)
            .then(async () => {
              return await addChips(gameId, chips, userId, msgId, chatId)
                .then(async () => {
                  // eslint-disable-next-line
                  console.log('ok?');

                  // timer for mongo add transaction
                  await (async () => {
                    return new Promise(async (resolve) => {
                      return setTimeout(() => {
                        resolve(true);
                      }, 100);
                    });
                  })();

                  return _gameReportCommand(bot, msg);
                })
                .catch((err) => {
                  // eslint-disable-next-line
                  console.error(err);
                });
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
            });
        }
      })
      .catch((err) => {
        if (err === false) {
          // eslint-disable-next-line
          console.log('Game not found, cant purchase');
          const resp =
            '⛔️ Игра не запущена, вносить фишки некуда.\nСначала начните новую игру.\nКоманда /start';
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
              reply_markup: {
                inline_keyboard: [[{ text: '/start', callback_data: 'start' }]],
              },
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        } else {
          // eslint-disable-next-line
          console.error(err);
          return err;
        }
      });
  } else return false;
};

export const inCommandExport = (
  bot: TelegramBot,
  msg: TelegramBot.Message,
  str: string
) => {
  return _inCommand(bot, msg, str);
};

export const inCommand = (bot: TelegramBot) => {
  bot.onText(/\/in (.+)/, async (msg, match) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const msgId = msg.message_id;
    const userName = msg.from.first_name;
    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _inCommand(bot, msg, msg.text);
    } else return false;
  });
};

const _outCommand = async (
  bot: TelegramBot,
  msg: TelegramBot.Message,
  str: string
) => {
  if (/\/out (.+)/.test(str)) {
    const match = str.match(/\/out (.+)/);
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const msgId = msg.message_id;
    _logCommand(`/out ${match[1]}`, msg);

    await getLiveGameByChatId(chatId)
      .then(async (liveGame: Game) => {
        const gameId = liveGame.id;
        const _badValue = () => {
          const resp = 'Неверное значение';
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        };

        const chips = Math.floor(Number(match[1]));
        if (isNaN(chips) || chips < 0 || chips > 999999) {
          return _badValue();
        } else {
          await addOrUpdateUser(msg)
            .then(async () => {
              await getUserPurchases(gameId, userId)
                .then(async (result) => {
                  if (result) {
                    await withdrawalChips(gameId, chips, userId, msgId, chatId)
                      .then(async () => {
                        // _meCommandBody(bot, msg);
                        _gameReportCommand(bot, msg);
                      })
                      .catch((err) => {
                        // eslint-disable-next-line
                        console.error(err);
                      });
                  } else {
                    const resp = `⛔️ Вы ещё ничего не вносили.\nНельзя выводить фишки до закупа.\nКоманда \`/in x\``;
                    bot
                      .sendMessage(chatId, resp, {
                        reply_to_message_id: msgId,
                        parse_mode: 'Markdown',
                      })
                      .catch(() => {
                        setTimeout(() => {
                          bot.sendMessage(chatId, resp, {
                            parse_mode: 'Markdown',
                          });
                        }, RETRY_AFTER);
                      });
                  }
                })
                .catch((err) => {
                  // eslint-disable-next-line
                  console.error(err);
                  const resp = `⛔️ Вы ещё ничего не вносили.\nНельзя выводить фишки до закупа.\nКоманда \`/in x\``;
                  bot
                    .sendMessage(chatId, resp, {
                      reply_to_message_id: msgId,
                      parse_mode: 'Markdown',
                    })
                    .catch(() => {
                      setTimeout(() => {
                        bot.sendMessage(chatId, resp, {
                          parse_mode: 'Markdown',
                        });
                      }, RETRY_AFTER);
                    });
                  return false;
                });
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
            });
        }
      })
      .catch((err) => {
        if (err === false) {
          // eslint-disable-next-line
          console.log('Game not found, cant withdrawal');
          const resp =
            '⛔️ Игра не запущена, вывести фишки неоткуда.\nСначала начните новую игру.\nКоманда /start';
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
              reply_markup: {
                inline_keyboard: [[{ text: '/start', callback_data: 'start' }]],
              },
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        } else {
          // eslint-disable-next-line
          console.error(err);
          return err;
        }
      });
  } else return false;
};

export const outCommandExport = (
  bot: TelegramBot,
  msg: TelegramBot.Message,
  str: string
) => {
  _outCommand(bot, msg, str);
};

export const outCommand = (bot: TelegramBot) => {
  bot.onText(/^\/out (.+)$/, async (msg, match) => {
    const chatId = msg.chat.id;
    const msgId = msg.message_id;
    const userId = msg.from.id;
    const userName = msg.from.first_name;
    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _outCommand(bot, msg, msg.text);
    } else return false;
  });
};

const _meCommandBody = async (bot: TelegramBot, msg: TelegramBot.Message) => {
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  const userName = msg.from.first_name;
  await getLiveGameByChatId(chatId)
    .then(async (liveGame: Game) => {
      const gameId = liveGame.id;
      await addOrUpdateUser(msg)
        .then(async () => {
          await getUserReport(gameId, userId)
            .then((report: UserSimpleReport) => {
              const resp = `\`\`\`\nЗакуп  ${report.in}\nВывод  ${report.out}\n=============\nБаланс ${report.balance}\`\`\``;
              bot
                .sendMessage(chatId, resp, {
                  reply_to_message_id: msgId,
                  parse_mode: 'Markdown',
                })
                .catch(() => {
                  setTimeout(() => {
                    bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                  }, RETRY_AFTER);
                });
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
              const resp = '⛔️ У тебя нет закупов в этой игре.';
              bot
                .sendMessage(chatId, resp, {
                  reply_to_message_id: msgId,
                  parse_mode: 'Markdown',
                })
                .catch(() => {
                  setTimeout(() => {
                    bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                  }, RETRY_AFTER);
                });
            });
        })
        .catch((err) => {
          // eslint-disable-next-line
          console.error(err);
        });
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
      const resp =
        '⛔️ Нет активной игры.\nСначала начните новую игру.\nКоманда /start';
      bot
        .sendMessage(chatId, resp, {
          reply_to_message_id: msgId,
          parse_mode: 'Markdown',
          reply_markup: {
            inline_keyboard: [[{ text: '/start', callback_data: 'start' }]],
          },
        })
        .catch(() => {
          setTimeout(() => {
            bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
          }, RETRY_AFTER);
        });
    });
};

export const meCommand = (bot: TelegramBot) => {
  bot.onText(/^\/me(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _logCommand(`/me`, msg);
      _meCommandBody(bot, msg);
    } else return false;
  });
};

const _statCommandBody = async (bot: TelegramBot, msg: TelegramBot.Message) => {
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  await getFullUserReport(userId)
    .then((result: UserSimpleReport) => {
      let resp = `Твоя общая статистика в боте:`;
      let globalIn = 0;
      let globalOut = 0;
      let globalBalance = 0;
      const inVal = result.in;
      globalIn += inVal;
      let inValStr = inVal.toString();
      while (inValStr.length < 9) {
        inValStr = inValStr + ' ';
      }
      const outVal = result.out;
      globalOut += isNaN(outVal) ? 0 : outVal;
      let outValStr = isNaN(outVal) ? '-' : outVal.toString();
      while (outValStr.length < 9) {
        outValStr = outValStr + ' ';
      }
      const balance = result.balance;
      globalBalance += balance;
      let balanceStr = balance.toString();
      while (balanceStr.length < 9) {
        balanceStr = balanceStr + ' ';
      }
      resp +=
        `\`\`\`\n` +
        `===========================\n` +
        `|Вход     |Выход    |Баланс   \n` +
        `|${inValStr}|${outValStr}|${balanceStr}\n` +
        `---------------------------\n` +
        `\`\`\`\n`;
      bot
        .sendMessage(chatId, resp, {
          reply_to_message_id: msgId,
          parse_mode: 'Markdown',
        })
        .catch(() => {
          setTimeout(() => {
            bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
          }, RETRY_AFTER);
        });
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
    });
};

const _statChatCommandBody = async (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  const userId = msg.from.id;
  let resp = '';
  await getLastGameUserReport(chatId, userId)
    .then((result: UserGameReport) => {
      const game = result.game;
      resp += `\n ㅤ\n⏮️ Твоя статистика в последней игре ${game.startTime.toLocaleDateString(
        'ru'
      )}:`;
      let globalIn = 0;
      let globalOut = 0;
      let globalBalance = 0;
      const inVal = result.in;
      globalIn += inVal;
      let inValStr = inVal.toString();
      while (inValStr.length < 9) {
        inValStr = inValStr + ' ';
      }
      const outVal = result.out;
      globalOut += isNaN(outVal) ? 0 : outVal;
      let outValStr = isNaN(outVal) ? '-' : outVal.toString();
      while (outValStr.length < 9) {
        outValStr = outValStr + ' ';
      }
      const balance = result.balance;
      globalBalance += balance;
      let balanceStr = balance.toString();
      while (balanceStr.length < 9) {
        balanceStr = balanceStr + ' ';
      }
      resp +=
        `\`\`\`\n` +
        `===========================\n` +
        `|Вход     |Выход    |Баланс   \n` +
        `|${inValStr}|${outValStr}|${balanceStr}\n` +
        `---------------------------\n` +
        `\`\`\``;
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
      resp += `\n ㅤ\n⏮️ Твоя статистика в последней игре:`;
      resp += `\`\`\`\nНе удалось получить статистику последней игры\`\`\`\n`;
    });

  resp += `\n📅 Твоя статистика за месяц (30дн):`;
  await getLastMonthUserReport(chatId, userId)
    .then((result: UserSimpleReport) => {
      let globalIn = 0;
      let globalOut = 0;
      let globalBalance = 0;
      const inVal = result.in;
      globalIn += inVal;
      let inValStr = inVal.toString();
      while (inValStr.length < 9) {
        inValStr = inValStr + ' ';
      }
      const outVal = result.out;
      globalOut += isNaN(outVal) ? 0 : outVal;
      let outValStr = isNaN(outVal) ? '-' : outVal.toString();
      while (outValStr.length < 9) {
        outValStr = outValStr + ' ';
      }
      const balance = result.balance;
      globalBalance += balance;
      let balanceStr = balance.toString();
      while (balanceStr.length < 9) {
        balanceStr = balanceStr + ' ';
      }
      resp +=
        `\`\`\`\n` +
        `===========================\n` +
        `|Вход     |Выход    |Баланс   \n` +
        `|${inValStr}|${outValStr}|${balanceStr}\n` +
        `---------------------------\n` +
        `\`\`\``;
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
      resp += `\`\`\`\nНе удалось получить статистику за месяц\`\`\`\n`;
    });

  resp += `\n🗓️ Твоя статистика за год (365дн):`;
  await getLastYearUserReport(chatId, userId)
    .then((result: UserSimpleReport) => {
      let globalIn = 0;
      let globalOut = 0;
      let globalBalance = 0;
      const inVal = result.in;
      globalIn += inVal;
      let inValStr = inVal.toString();
      while (inValStr.length < 9) {
        inValStr = inValStr + ' ';
      }
      const outVal = result.out;
      globalOut += isNaN(outVal) ? 0 : outVal;
      let outValStr = isNaN(outVal) ? '-' : outVal.toString();
      while (outValStr.length < 9) {
        outValStr = outValStr + ' ';
      }
      const balance = result.balance;
      globalBalance += balance;
      let balanceStr = balance.toString();
      while (balanceStr.length < 9) {
        balanceStr = balanceStr + ' ';
      }
      resp +=
        `\`\`\`\n` +
        `===========================\n` +
        `|Вход     |Выход    |Баланс   \n` +
        `|${inValStr}|${outValStr}|${balanceStr}\n` +
        `---------------------------\n` +
        `\`\`\``;
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
      resp += `\`\`\`\nНе удалось получить статистику за год\`\`\`\n`;
    });

  resp += `\n💯 Твоя статистика в этом чате за всё время:`;
  await getFullChatUserReport(chatId, userId)
    .then((result: UserSimpleReport) => {
      let globalIn = 0;
      let globalOut = 0;
      let globalBalance = 0;
      const inVal = result.in;
      globalIn += inVal;
      let inValStr = inVal.toString();
      while (inValStr.length < 9) {
        inValStr = inValStr + ' ';
      }
      const outVal = result.out;
      globalOut += isNaN(outVal) ? 0 : outVal;
      let outValStr = isNaN(outVal) ? '-' : outVal.toString();
      while (outValStr.length < 9) {
        outValStr = outValStr + ' ';
      }
      const balance = result.balance;
      globalBalance += balance;
      let balanceStr = balance.toString();
      while (balanceStr.length < 9) {
        balanceStr = balanceStr + ' ';
      }
      resp +=
        `\`\`\`\n` +
        `===========================\n` +
        `|Вход     |Выход    |Баланс   \n` +
        `|${inValStr}|${outValStr}|${balanceStr}\n` +
        `---------------------------\n` +
        `\`\`\`\n`;
      bot
        .sendMessage(chatId, resp, {
          reply_to_message_id: msgId,
          parse_mode: 'Markdown',
        })
        .catch(() => {
          setTimeout(() => {
            bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
          }, RETRY_AFTER);
        });
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
      resp += `\`\`\`\nНе удалось получить статистику в этом чате\`\`\`\n`;
    });
};

export const statCommand = (bot: TelegramBot) => {
  bot.onText(/^\/stat(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _logCommand(`/stat`, msg);
      _statChatCommandBody(bot, msg);
    } else return false;
  });
};

export const deleteCommand = (bot: TelegramBot) => {
  bot.onText(/^\/delete(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const msgId = msg.message_id;
    const userId = msg.from.id;
    const messageIdForDelete = msg.reply_to_message?.message_id;

    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _logCommand(`/delete`, msg);
      if (!messageIdForDelete) {
        const resp = `⛔️ Для удаления транзакции надо сделать реплай транзакции игрока.`;
        bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            parse_mode: 'Markdown',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
            }, RETRY_AFTER);
          });
        return;
      }

      await getLiveGameByChatId(chatId)
        .then(async (liveGame: Game) => {
          const gameId = liveGame.id;
          await addOrUpdateUser(msg)
            .then(async () => {
              if (liveGame.admins.includes(userId)) {
                await deleteTransaction(gameId, messageIdForDelete)
                  .then(() => {
                    const resp = '🟢 Удалено!';
                    bot
                      .sendMessage(chatId, resp, {
                        reply_to_message_id: msgId,
                        parse_mode: 'Markdown',
                      })
                      .catch(() => {
                        setTimeout(() => {
                          bot.sendMessage(chatId, resp, {
                            parse_mode: 'Markdown',
                          });
                        }, RETRY_AFTER);
                      });
                    _gameReportCommand(bot, msg);
                  })
                  .catch((err) => {
                    // eslint-disable-next-line
                    console.log('catch');
                    // eslint-disable-next-line
                    console.log(err);
                    const resp = '⛔️ Транзакция не найдена';
                    bot
                      .sendMessage(chatId, resp, {
                        reply_to_message_id: msgId,
                        parse_mode: 'Markdown',
                      })
                      .catch(() => {
                        setTimeout(() => {
                          bot.sendMessage(chatId, resp, {
                            parse_mode: 'Markdown',
                          });
                        }, RETRY_AFTER);
                      });
                  });
              } else {
                const adminMentionNames = await getAdminsStr(liveGame);
                const resp = `⛔️ Нет прав для удаления.\nПопросите админа удалить транзакцию.\n${_getAdminsStr(
                  adminMentionNames
                )}`;
                bot
                  .sendMessage(chatId, resp, {
                    reply_to_message_id: msgId,
                    parse_mode: 'Markdown',
                  })
                  .catch(() => {
                    setTimeout(() => {
                      bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                    }, RETRY_AFTER);
                  });
              }
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
            });
        })
        .catch((err) => {
          // eslint-disable-next-line
          console.error(err);
          const resp =
            '⛔️ Нет активной игры.\nСначала начните новую игру.\nКоманда /start';
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
              reply_markup: {
                inline_keyboard: [[{ text: '/start', callback_data: 'start' }]],
              },
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        });
    } else return false;
  });
};

const _gameReportCommand = async (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  if (!msg || !msg.chat) {
    return;
  }
  const chatId = msg.chat.id;
  const msgId = msg.message_id;
  // const userId = msg.from.id;

  let resultedBalance = 0;

  return await getLiveGameByChatId(chatId)
    .then(async (liveGame: Game) => {
      const gameId = liveGame.id;
      const gameStartedTime = liveGame.startTime;
      return await addOrUpdateUser(msg)
        .then(async () => {
          return await getGameReport(liveGame.id, liveGame.silent, true)
            .then(async (result: CurrentReport) => {
              // eslint-disable-next-line
              console.log('getGameReport', result);
              const sortedActive = result.entities
                // .filter((a) => a.name[0] === '[')
                .filter((a) => a.status === PlayerStatuses.active)
                .sort((a, b) => {
                  if (a.summaryIn < b.summaryIn) {
                    return 1;
                  } else return -1;
                });
              const sortedEnded = result.entities
                // .filter((a) => a.name[0] === '*')
                .filter((a) => a.status === PlayerStatuses.ended)
                .sort((a, b) => {
                  if (a.summary > b.summary) {
                    return 1;
                  } else return -1;
                });
              const sortedResult = sortedActive.concat(sortedEnded);
              const countActive = sortedActive.length;
              const countEnded = sortedEnded.length;
              let resp = ``;
              let globalIn = 0;
              let globalOut = 0;
              let globalBalance = 0;
              for (let index = 0; index < sortedResult.length; index++) {
                const entity = sortedResult[index];
                const name = entity.name;
                const inVal = entity.summaryIn;
                globalIn += inVal;
                let inValStr = inVal.toString();
                while (inValStr.length < 8) {
                  inValStr = inValStr + ' ';
                }
                const outVal = entity.summaryOut;
                globalOut += isNaN(outVal) ? 0 : outVal;
                let outValStr = isNaN(outVal) ? '-' : outVal.toString();
                while (outValStr.length < 8) {
                  outValStr = outValStr + ' ';
                }
                const balance = entity.summary;
                globalBalance += balance;
                let balanceStr = balance.toString();
                while (balanceStr.length < 8) {
                  balanceStr = balanceStr + ' ';
                }
                resp +=
                  `${name}\n` +
                  `\`\`\`\n` +
                  `===========================\n` +
                  `|Вход    |Выход   |Баланс  \n` +
                  `|${inValStr}|${outValStr}|${balanceStr}${
                    PlayerStatusesEmoji[entity.status]
                  }\n` +
                  `---------------------------\n` +
                  `\`\`\`\n`;
              }
              // last purchase
              const lastIns = result.lastIns;
              let lastPurchase = '-\n';
              if (lastIns && lastIns.length) {
                lastPurchase = `\n⚠️ Последние докупы:\n`;
                for (let index = 0; index < lastIns.length; index++) {
                  const element = lastIns[index];
                  lastPurchase += `${element.time} | ${element.name} | ${element.in}\n`;
                }
              }
              // seats
              let seatsStat = `Посадка:\n`;
              let seatSymbol = '⬜️';
              if (countActive > 0) {
                if (countActive <= 6) {
                  seatSymbol = '🟩';
                } else if (countActive < 10) {
                  seatSymbol = '🟨';
                } else if (countActive >= 10) {
                  seatSymbol = '🟥';
                }
              }
              for (let index = 0; index < countActive; index++) {
                seatsStat += seatSymbol;
                if (index === 4) {
                  seatsStat += ' ';
                }
              }
              if (countActive < 10) {
                for (let index = 0; index < 10 - countActive; index++) {
                  seatsStat += `⬜️`;
                  if (countActive + index === 4) {
                    seatsStat += ` `;
                  }
                }
              }
              // users stats
              seatsStat += `\n\n`;
              seatsStat += `🙋 В игре: ${countActive}\n🙅 Вышло игроков: ${countEnded}\n💁 Всего за игру: ${
                countActive + countEnded
              }`;
              seatsStat += `\n\n`;
              // time stats
              const deltaTime =
                new Date().getTime() - new Date(gameStartedTime).getTime();
              const deltaSecondsAll = Math.floor(deltaTime / 1000);
              const deltaMinutes = Math.floor(deltaSecondsAll / 60) % 60;
              const deltaHours = Math.floor(deltaSecondsAll / (60 * 60));
              const deltaSeconds =
                deltaSecondsAll - deltaMinutes * 60 - deltaHours * 60 * 60;
              const deltaTimeStr = `${deltaHours}:${
                deltaMinutes >= 10 ? deltaMinutes : '0' + deltaMinutes
              }:${deltaSeconds >= 10 ? deltaSeconds : '0' + deltaSeconds}`;

              // TODO: fix datetime locale
              const startDay = new Date(gameStartedTime).getDay();

              const timeStat = `⏱ Начало ${new Date(
                gameStartedTime
              ).toLocaleString('ru-RU')} \n⏱ Длительность ${deltaTimeStr}\n`;

              const isSilent = liveGame.silent;
              // eslint-disable-next-line
              console.log('liveGame', liveGame);

              const silentReport = `\n${
                isSilent ? '🔇 тихий режим *ON*' : '🔊 тихий режим *OFF*'
              }\n`;

              resp +=
                seatsStat +
                timeStat +
                silentReport +
                lastPurchase +
                `\n*### ИТОГ ###*\n` +
                `\`\`\`\n` +
                `Вход - Выход = 0 ?\n` +
                `${globalIn} - ${globalOut} = ${globalBalance}\n` +
                `\`\`\`\n`;
              resultedBalance = globalBalance;
              const btns = await inOutBtns(chatId);
              let lastRowBtns = [];
              if (countActive === 0) {
                lastRowBtns.push({ text: '❌ /stop', callback_data: 'stop' });
                if (globalBalance !== 0) {
                  lastRowBtns.push({
                    text: '👮‍♂️ /kill',
                    callback_data: 'kill',
                  });
                }
                lastRowBtns.push({
                  text: '🧮 /calc',
                  callback_data: `calc ${liveGame.id}`,
                });
              }
              btns.inline_keyboard.push(lastRowBtns);
              bot
                .sendMessage(chatId, resp, {
                  reply_to_message_id: msgId,
                  parse_mode: 'Markdown',
                  reply_markup: btns,
                })
                .catch(() => {
                  setTimeout(() => {
                    bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                  }, RETRY_AFTER);
                });
              return resultedBalance;
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
              const resp = '⛔️ Нет закупов в текущей игре.';
              bot
                .sendMessage(chatId, resp, {
                  reply_to_message_id: msgId,
                  parse_mode: 'Markdown',
                })
                .catch(() => {
                  setTimeout(() => {
                    bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                  }, RETRY_AFTER);
                });
              if (err === 'No transactions') {
                return 'No transactions';
              } else {
                return false;
              }
            });
        })
        .catch((err) => {
          // eslint-disable-next-line
          console.error(err);
          return false;
        });
    })
    .catch((err) => {
      // eslint-disable-next-line
      console.error(err);
      const resp =
        '⛔️ Нет активной игры.\nСначала начните новую игру.\nКоманда /start';
      bot
        .sendMessage(chatId, resp, {
          reply_to_message_id: msgId,
          parse_mode: 'Markdown',
          reply_markup: {
            inline_keyboard: [[{ text: '/start', callback_data: 'start' }]],
          },
        })
        .catch(() => {
          setTimeout(() => {
            bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
          }, RETRY_AFTER);
        });
      return 'No live game';
    });
};

export const gameReportCommand = (bot: TelegramBot) => {
  bot.onText(
    /^\/report(@poker_money_counter_bot)?$/,
    async (msg: TelegramBot.Message) => {
      const chatId = msg.chat.id;
      const userId = msg.from.id;
      if ((devMode && userId === devId && chatId === devId) || !devMode) {
        _logCommand(`/report`, msg);

        await _gameReportCommand(bot, msg);
      } else return false;
    }
  );
};

export const gameReportCommandExport = async (
  bot: TelegramBot,
  msg: TelegramBot.Message
) => {
  await _gameReportCommand(bot, msg);
};

export const helpCommand = (bot: TelegramBot) => {
  bot.onText(/^\/help(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const msgId = msg.message_id;

    _logCommand(`/help`, msg);
    const resp = `Бот для подсчета входов и выходов фишек.
Команды:
/help - эта инструкция
/start - начало игры; запустивший игру становится админом
/stop - окончание игры; может выполнить только админ
/in x - ввод фишек; x - число, кол-во фишек
/out x - вывод фишек
/delete - удаление транзакции; может выполнить только админ
/me - текущий баланс игрока
/report - полный список докупов и выводов в текущей игре`;
    return bot
      .sendMessage(chatId, resp, {
        reply_to_message_id: msgId,
        parse_mode: 'Markdown',
      })
      .catch(() => {
        setTimeout(() => {
          bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
        }, RETRY_AFTER);
      });
  });
};

function splitString(
  s: string,
  maxSymbols: number,
  delimiter: string = '---'
): string[] {
  const chunks: string[] = [];
  let remaining = s;

  while (remaining.length > maxSymbols) {
    let index = remaining.indexOf(delimiter);
    if (index === -1 || index > maxSymbols) {
      break;
    }
    chunks.push(remaining.substring(0, index));
    remaining = remaining.substring(index + delimiter.length);
  }

  return chunks;
}

export const liveGamesCommand = (bot: TelegramBot) => {
  bot.onText(/^\/livegames(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const msgId = msg.message_id;

    _logCommand(`/livegames`, msg);
    if (userId.toString() !== config.devUserId) {
      return false;
    } else {
      await getAllLiveGames()
        .then(async (result) => {
          if (isArray(result) && result.length) {
            const games = [];
            let resp = `${result.length} live games found!\n`;
            for (let index = 0; index < result.length; index++) {
              const game = result[index];

              resp += `---\n${result.length - index}) игра ${
                game.id
              }\nначата: ${game.startTime.toLocaleString('ru-RU')}\nамдин(ы): `;

              for (let i = 0; i < game.admins.length; i++) {
                let userName = 'admin';
                await getUserById(game.admins[i])
                  .then((user) => {
                    userName = user.name;
                  })
                  .catch((err) => {
                    // eslint-disable-next-line
                    console.error(err);
                  });
                resp += `[@${userName}](tg://user?id=${game.admins[i]}) `;
              }

              resp += `чат: [${game.chatId}](tg://chat?id=${game.chatId})`;
              // resp += `чат: [${game.chatId}](t.me/c/${game.chatId}/${game.startMessageId})`;

              resp += `\n`;
            }
            // resp += `${JSON.stringify(result, undefined, 2)}`;
            const respLength = resp.length;
            console.log('respLength', respLength);
            console.log('games.length', result.length);
            if (respLength > 4096) {
              let resp2 = `Message too long to send in one message\n`;
              resp2 += `This is some of them:\n`;
              resp2 += splitString(resp, 3800).join('---') + '...';
              return bot
                .sendMessage(chatId, resp2, {
                  reply_to_message_id: msgId,
                  parse_mode: 'Markdown',
                })
                .catch(() => {
                  setTimeout(() => {
                    bot.sendMessage(chatId, resp2, { parse_mode: 'Markdown' });
                  }, RETRY_AFTER);
                });
            } else
              return bot
                .sendMessage(chatId, resp, {
                  reply_to_message_id: msgId,
                  parse_mode: 'Markdown',
                })
                .catch(() => {
                  setTimeout(() => {
                    bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                  }, RETRY_AFTER);
                });
          } else {
            const resp = 'No live games';
            return bot
              .sendMessage(chatId, resp, {
                reply_to_message_id: msgId,
                parse_mode: 'Markdown',
              })
              .catch(() => {
                setTimeout(() => {
                  bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                }, RETRY_AFTER);
              });
          }
        })
        .catch((err) => {
          const resp = `No live games
${JSON.stringify(err, undefined, 2)}`;
          return bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'HTML',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        });
    }
  });
};

// TODO: finish command
export const addAdminCommand = (bot: TelegramBot) => {
  bot.onText(/^\/addadmin(@poker_money_counter_bot)?$/, async (msg) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const msgId = msg.message_id;

    const messageIdForAddAdmin = msg.reply_to_message?.message_id;

    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _logCommand(`/addadmin`, msg);

      if (!messageIdForAddAdmin) {
        const resp = `⛔️ Для добавления админа нужно сделать реплай сообщения игрока.`;
        bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            parse_mode: 'Markdown',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
            }, RETRY_AFTER);
          });
        return;
      }

      await getLiveGameByChatId(chatId)
        .then(async (liveGame: Game) => {
          const gameId = liveGame.id;
          await addOrUpdateUser(msg)
            .then(async () => {
              if (liveGame.admins.includes(userId)) {
                // eslint-disable-next-line
                console.log('WIP: may add admin');
              } else {
                const adminMentionNames = await getAdminsStr(liveGame);
                const resp = `⛔️ Нет прав для добавления админов.\nПопросите админа.\n${_getAdminsStr(
                  adminMentionNames
                )}`;
                bot
                  .sendMessage(chatId, resp, {
                    reply_to_message_id: msgId,
                    parse_mode: 'Markdown',
                  })
                  .catch(() => {
                    setTimeout(() => {
                      bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
                    }, RETRY_AFTER);
                  });
              }
            })
            .catch((err) => {
              // eslint-disable-next-line
              console.error(err);
            });
        })
        .catch((err) => {
          // eslint-disable-next-line
          console.error(err);
          const resp =
            '⛔️ Нет активной игры.\nСначала начните новую игру.\nКоманда /start';
          bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'Markdown',
              reply_markup: {
                inline_keyboard: [[{ text: '/start', callback_data: 'start' }]],
              },
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        });
    } else return false;
  });
};

export const msgInfoCommand = (bot: TelegramBot) => {
  bot.onText(/^\/log(@poker_money_counter_bot)? (.+)?$/, async (msg, match) => {
    const chatId = msg.chat.id;
    const userId = msg.from.id;
    const msgId = msg.message_id;

    if ((devMode && userId === devId && chatId === devId) || !devMode) {
      _logCommand(`/log`, msg);
      if (userId.toString() !== config.devUserId) {
        return false;
      } else {
        const msgData = JSON.stringify(msg, undefined, 4);
        const resp = msgData;
        return bot
          .sendMessage(chatId, resp, {
            reply_to_message_id: msgId,
            parse_mode: 'HTML',
          })
          .catch(() => {
            setTimeout(() => {
              bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
            }, RETRY_AFTER);
          });
      }
    } else return false;
  });
};

export const developmentInProgressCommand = (bot: TelegramBot) => {
  return devMode
    ? bot.onText(/^(.*?)$/, async (msg) => {
        const chatId = msg.chat.id;
        const userId = msg.from.id;
        const msgId = msg.message_id;

        _logCommand('dev mode', msg);

        if (chatId !== devId || userId !== devId) {
          const resp = 'Ведётся разработка, бот на паузе.';
          return bot
            .sendMessage(chatId, resp, {
              reply_to_message_id: msgId,
              parse_mode: 'HTML',
            })
            .catch(() => {
              setTimeout(() => {
                bot.sendMessage(chatId, resp, { parse_mode: 'Markdown' });
              }, RETRY_AFTER);
            });
        }
      })
    : false;
};

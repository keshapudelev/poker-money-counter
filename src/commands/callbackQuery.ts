import TelegramBot = require('node-telegram-bot-api');
import { getUsersStr } from '../db_actions/games.actions';
import {
  calcCommandExport,
  inCommandExport,
  killCommandExport,
  outCommandExport,
  startCommandExport,
  stopCommandExport,
} from './gameCommands';
import { isNumber } from 'util';

export const callbackQueryAction = async (bot: TelegramBot) => {
  bot.on('callback_query', async (query) => {
    const data = query.data;
    const message = query.message;
    // eslint-disable-next-line
    console.log(query);

    const opts = {
      chat_id: message.chat.id,
      reply_to_message_id: message.reply_to_message.message_id,
    };

    const userId = query.from.id;
    const userNameStr = await getUsersStr([userId]);
    let value;
    let command;
    const commandArr = data.split('_');

    // in/out

    if (commandArr[0] === 'in') {
      command = 'in';
      value = Number(commandArr[1]);
    } else {
      if (commandArr[0] === 'out') {
        command = 'out';
        value = 0 - Number(commandArr[1]);
      }
    }
    if ((value || value === 0) && (command === 'in' || command === 'out')) {
      const commandStr = command === 'in' ? 'добавлено' : 'выход';
      bot.answerCallbackQuery(query.id).then(() => {
        bot
          .editMessageReplyMarkup(
            { inline_keyboard: [] },
            { chat_id: opts.chat_id, message_id: message.message_id }
          )
          .then(() => {
            bot
              .sendMessage(
                opts.chat_id,
                `${userNameStr}: ${commandStr} ${value}`,
                {
                  parse_mode: 'Markdown',
                }
              )
              .then((msg: TelegramBot.Message) => {
                msg.from = query.from;
                if (command === 'in') {
                  inCommandExport(bot, msg, `/${command} ${value}`);
                } else if (command === 'out') {
                  outCommandExport(bot, msg, `/${command} ${value}`);
                }
              });
          });
      });
    }

    // start

    if (commandArr[0] === 'start') {
      command = 'start';
    }

    if (command === 'start') {
      const commandStr = 'начал игру';
      bot.answerCallbackQuery(query.id).then(() => {
        bot
          .editMessageReplyMarkup(
            { inline_keyboard: [] },
            { chat_id: opts.chat_id, message_id: message.message_id }
          )
          .then(() => {
            bot
              .sendMessage(opts.chat_id, `${userNameStr} ${commandStr}`, {
                parse_mode: 'Markdown',
              })
              .then((msg: TelegramBot.Message) => {
                msg.from = query.from;
                if (command === 'start') {
                  startCommandExport(bot, msg);
                }
              });
          });
      });
    }

    // stop

    if (commandArr[0] === 'stop') {
      command = 'stop';
    }

    if (command === 'stop') {
      const commandStr = 'заканчивает игру';
      bot.answerCallbackQuery(query.id).then(() => {
        bot
          .editMessageReplyMarkup(
            { inline_keyboard: [] },
            { chat_id: opts.chat_id, message_id: message.message_id }
          )
          .then(() => {
            bot
              .sendMessage(opts.chat_id, `${userNameStr} ${commandStr}`, {
                parse_mode: 'Markdown',
              })
              .then((msg: TelegramBot.Message) => {
                msg.from = query.from;
                if (command === 'stop') {
                  stopCommandExport(bot, msg);
                }
              });
          });
      });
    }

    // kill

    if (commandArr[0] === 'kill') {
      command = 'kill';
    }

    if (command === 'kill') {
      const commandStr = 'закрыл рассчеты, закончил игру.';
      bot.answerCallbackQuery(query.id).then(() => {
        bot
          .editMessageReplyMarkup(
            { inline_keyboard: [] },
            { chat_id: opts.chat_id, message_id: message.message_id }
          )
          .then(() => {
            bot
              .sendMessage(opts.chat_id, `${userNameStr} ${commandStr}`, {
                parse_mode: 'Markdown',
              })
              .then((msg: TelegramBot.Message) => {
                msg.from = query.from;
                if (command === 'kill') {
                  killCommandExport(bot, msg);
                }
              });
          });
      });
    }

    // count

    if (
      commandArr[0] === 'calc' ||
      (commandArr[0]?.split(' ')?.[0] === 'calc' &&
        typeof Number(commandArr[0]?.split(' ')?.[1]) === 'number' &&
        !isNaN(Number(commandArr[0]?.split(' ')?.[1])))
    ) {
      command = 'calc';
    }

    if (command === 'calc') {
      const commandStr = 'запустил расчёт';
      bot.answerCallbackQuery(query.id).then(() => {
        bot
          .editMessageReplyMarkup(
            { inline_keyboard: [] },
            { chat_id: opts.chat_id, message_id: message.message_id }
          )
          .then(() => {
            bot
              .sendMessage(opts.chat_id, `${userNameStr} ${commandStr}`, {
                parse_mode: 'Markdown',
              })
              .then((msg: TelegramBot.Message) => {
                msg.from = query.from;
                msg.text = commandArr[0];
                if (command === 'calc') {
                  calcCommandExport(bot, msg);
                }
              });
          });
      });
    }

    return;
  });
};
